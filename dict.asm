global find_word
extern string_equals

section .text
; Два аргумента:
; в rdi - указатель на нуль-терминированную строку;
; в rsi - указатель на начало словаря.
; Возвращает в rax адрес начала вхождения в словарь или 0.

find_word:
    xor rax, rax
    test rsi, rsi
    je .exit

    push rsi
    add rsi, 8
    push rdi
    call string_equals
    pop rdi
    pop rsi

    test rax, rax
    jne .found
    mov rsi, [rsi]
    jmp find_word

.found:
    mov rax, rsi

.exit:
    ret

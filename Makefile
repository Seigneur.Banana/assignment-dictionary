ASM_FLAGS=-felf64
ASM=nasm

main: main.o dict.o lib.o
	ld -o main main.o dict.o lib.o

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASM_FLAGS) main.asm

dict.o: dict.asm
	$(ASM) $(ASM_FLAGS) dict.asm

lib.o: lib.asm
	$(ASM) $(ASM_FLAGS) lib.asm

clean:
	rm -rf main main.o dict.o lib.o

.PHONY: clean

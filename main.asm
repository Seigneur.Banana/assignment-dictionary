%include "colon.inc"
%include "words.inc"

extern print_string
extern print_error
extern read_word
extern find_word
extern string_length
extern print_newline
extern exit

global _start

section .rodata

input_key: db "Введите ключ: ", 0
found: db "Найдено значение: ", 0
no_found: db "Такого ключа не существует.", 10, 0
too_long_key: db "Длина ключа превышает 256 символов!", 10, 0

section .text
_start:
    mov rdi, input_key
    call print_string

    mov rsi, 256
    sub rsp, 256
    mov rdi, rsp
    call read_word
    test rax, rax
    jz .too_long
    mov rdi, rax
    mov rsi, into
    call find_word
    test rax, rax
    jz .not_found
    add rsp, 256
    add rax, 8

    push rax
    mov rdi, found
    call print_string
    pop rdi
    push rdi
    call string_length
    pop rdi

    inc rax
    add rdi, rax
    call print_string
    call print_newline
    call exit

.not_found:
    add rsp, 256
    mov rdi, no_found
    call print_error
    call exit

.too_long:
    add rsp, 256
    mov rdi, too_long_key
    call print_error
    call exit


